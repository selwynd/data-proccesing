'''These final rules are used to visualize the results.
This is done by making a bar plot of the top 10 most found pathogens'''

rule covert_to_txt:
    input:
            potential_pathogen = "Results/{sample}.sam",
            pathogen = config["large_data_dir"] + config["input"]
    output:
            "{sample}.txt",
            "{sample}_raw.txt"
    message:
            "Now coverting sam file into txt with simple layout"
    log:
        "log/visualize/visual.log"
    script:
        'Scripts/sam_to_txt.py'

rule visualize:
    input:
        "{sample}_raw.txt"
    output:
        "{sample}_Results.jpg"
    message:
            "Now creating bar plots for the samples"
    log:
        temp(config["large_data_dir"] + "log/visual/barplot.log")
    shell:
        "Rscript Scripts/make_histo.R {input} {output} > {log}"
