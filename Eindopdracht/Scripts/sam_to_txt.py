#!/usr/bin/env python

def accession_reader(accession, pathogen_file):
    '''This function will use the accession number to search in the 
    pathogen file for the matching header.'''
    fasta = open(pathogen_file)
    for line in fasta:
            if accession in line and line.startswith(">"):
                    acc = ''.join(line)
                    acc = acc[13:]
                    accession_header = acc.split(',')[0]
                    return accession_header             
                        
def read_results():
    '''This function will read the results from the .sam files and 
    output them into a new file with the same name (.txt.)'''
    mydict = dict()
    out_raw = open(snakemake.output[1], 'w')
    with open(snakemake.output[0], 'w') as out:
        for i in snakemake.input:
            if i.endswith('.fasta'):
                pathogen_file = i
                flag = True
            else:
                pass
        for i in snakemake.input:
            if i.endswith('.sam') and flag==True:
                print('These are the results of sample:',i,'The more hits a single pathogen gets the more likely it is present in the DNA sample', file=out)
                print('Accession', "\t", 'Hits', "\t", 'Header', file=out)
                for line in open(i):
                    if line.startswith("@"):
                       pass
                    else:
                       item = line.split()[2]
                       if item not in mydict:
                           mydict[item] = 1
                       else:
                           mydict[item] += 1                
                for pathogen in sorted(mydict.items(), key=lambda x:x[1],reverse=True):
                    print(pathogen[0], "\t", pathogen[1], "\t", accession_reader(pathogen[0], pathogen_file), file=out)
                    
                    print(pathogen[0], ";", pathogen[1], ";", accession_reader(pathogen[0], pathogen_file), file=out_raw)
                out.close()
                out_raw.close()

read_results()

