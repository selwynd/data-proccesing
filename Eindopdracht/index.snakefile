'''These 2 rules are used to create the index of the pathogen file and the 
reference genome. This step will take the most time. (aprox 5 hours)'''

rule make_bowtie_index:
    input: 
            pathogen = config["large_data_dir"] + config["input"]
    output: 
            expand(config["large_data_dir"]+"index_pathogen.{num}.bt2", num=range(1,4)),
            expand(config["large_data_dir"]+"index_pathogen.rev.{num}.bt2", num=range(1,2))
    threads:64
    log:
        temp(config["large_data_dir"] + "log/index/pathogen.log")
    message:
            "First a index of the pathogen file will be created"
    shell:
        "bowtie2-build {input} /data/storix2/student/Thema11/sdijkstra/index_pathogen -p {threads} > {log}"
        
rule make_genome_reference:
    input:
            "/commons/student/2017-2018/Thema11/Sdijkstra/complete_human_genome.fasta"
    output:
            expand(config["large_data_dir"]+"hgenome.{num}.bt2", num=range(1,4)),
            expand(config["large_data_dir"]+"hgenome.rev.{num}.bt2", num=range(1,2))
    message:
            "Now making an index for the human genome"
    log:
        temp(config["large_data_dir"] + "log/index/human_ref.log")
    threads:64
    shell:
            "bowtie2-build {input} /data/storix2/student/Thema11/sdijkstra/hgenome -p {threads}"
