'''These rules will use mapping to find the foreign DNA in the samples
this foreign DNA will be mapped against a pathogen file containing a 
big database of viruses and other pathogens.'''

rule align_human_reference:
    input: 
            b_1 = config["sample_dir"] + "{sample}_R1.fastq",
            b_2 = config["sample_dir"] + "{sample}_R2.fastq"
    
    output:
            sam = config["large_data_dir"] + "Results/FailALign{sample}.sam",
            pathogen_1 = "Results/FailALign{sample}.1.fastq",
            pathogen_2 = "Results/FailALign{sample}.2.fastq"
    threads: 64
    message:
            "Mapping the sample against the human reference genome"
    log:
        temp(config["large_data_dir"] + "mapping/sample-human_ref.log")
    shell:
         "bowtie2 --un-conc Results/FailALign{wildcards.sample}.fastq -p {threads} -x /data/storix2/student/Thema11/sdijkstra/hgenome -1 {input.b_1} -2 {input.b_2} -S {output.sam} --no-unal > {log}"


rule align_pathogens:
    input:
            pathogen_1 = "Results/FailALign{sample}.1.fastq",
            pathogen_2 = "Results/FailALign{sample}.2.fastq"
    output:
            "Results/{sample}.sam"
    threads: 64
    message:
            "Now mapping the foreign DNA against the pathogen file"
    log:
        temp(config["large_data_dir"]+"log/mapping/align_pathogen.log")
    shell:
            "bowtie2 -x /data/storix2/student/Thema11/sdijkstra/index_pathogen -1 {input.pathogen_1} -2 {input.pathogen_2} -p {threads} -S {output}"
