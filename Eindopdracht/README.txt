This readme explains the function and use of this pipeline.
Selwyn Dijkstra
BFV-3

Function

This pipeline is used to visualise the amount of pathogens found inside a given human DNA sample.
In this itteration a DNA sample from a deceased humans brain is used. First a list of most known pathogens is indexed 
with bowtie2. Then a human reference genome is created. After that the DNA samples will be mapped against the reference genome
all the DNA that did not match will be saved inside a file. This remaining DNA will get mapped against the pathogen index.
This results in a file containing all the matches of pathogens with each sample. This results will be visualized in a histogram.
In theorie all human DNA samples can be used to find pathogens using this pipeline.

Use

1. ssh assemblix (a lot of proccessing power is needed for the creating of indexes and mapping.)
2. source venv/bin/activate (use a virtualenv to prevent missing packages)
2. snakemake --snakefile pipeline --cores 32 (32 cores are selected to speed up the processing time)