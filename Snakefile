SAMPLES = ['Sample1', 'Sample2', 'Sample3']

rule all:
    input:
        'final.txt'
        

rule quantify_genes:
    input:
        genome = 'genome.fa',
        r1 = 'data/{sample}.R1.fastq.gz',
        r2 = 'data/{sample}.R2.fastq.gz'
    output:
        '{sample}.txt'
    shell:
        'echo {input.genome} {input.r1} {input.r2} > {output}'

rule clean:
	shell:
		'rm *.txt'

rule merge:
	input:
		expand('{sample}.txt', sample = SAMPLES)
	output:
		'final.txt'
	shell:
		'cat {input} >> {output}'
	
